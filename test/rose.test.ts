import GildedRose, { Item, ItemType } from '../src';

// Utility function to retrieve item
function getItem(store: GildedRose, name: string) {
  for (let i = 0; i < store.items.length; i++) {
    const item = store.items[i];
    if (item.name === name) {
      return item;
    }
  }

  return null;
}

describe('update', () => {
  it('works', () => {
    expect(new GildedRose()).not.toBeNull();
  });

  it('Sulfuras does not degrade', () => {
    let store = new GildedRose();
    store.updateQuality();

    let sulfuras = getItem(store, 'Sulfuras, Hand of Ragnaros');
    expect(sulfuras?.quality).toEqual(80);
  });

  it('Sulfuras does not decrease sellIn', () => {
    let store = new GildedRose();
    store.updateQuality();

    let sulfuras = getItem(store, 'Sulfuras, Hand of Ragnaros');
    expect(sulfuras?.sellIn).toEqual(0);
  });

  it('Aged Brie increases in quality', () => {
    let store = new GildedRose();

    store.updateQuality();

    let updatedAgedBrie = getItem(store, 'Aged Brie');

    expect(updatedAgedBrie?.quality).toEqual(1);
  });

  it('Quality does not increase > 50', () => {
    let store = new GildedRose();

    // Update 100 times
    for (let i = 0; i < 100; i++) {
      store.updateQuality();
    }

    let updatedAgedBrie = getItem(store, 'Aged Brie');

    expect(updatedAgedBrie?.quality).toEqual(50);
  });

  it('Backstage passes increase by 1, 2, 3 increments based on time', () => {
    let store = new GildedRose();

    for (let i = 0; i < 5; i++) {
      store.updateQuality();
    }

    let pass = getItem(store, 'Backstage passes to a TAFKAL80ETC concert');

    expect(pass?.quality).toEqual(25);

    for (let i = 0; i < 5; i++) {
      store.updateQuality();
    }

    pass = getItem(store, 'Backstage passes to a TAFKAL80ETC concert');

    expect(pass?.quality).toEqual(35);

    for (let i = 0; i < 5; i++) {
      store.updateQuality();
    }

    pass = getItem(store, 'Backstage passes to a TAFKAL80ETC concert');

    expect(pass?.quality).toEqual(50);
  });

  it('Backstage passes 0 quality after concert day', () => {
    let store = new GildedRose();

    for (let i = 0; i < 16; i++) {
      store.updateQuality();
    }

    let pass = getItem(store, 'Backstage passes to a TAFKAL80ETC concert');

    expect(pass?.quality).toEqual(0);
  });

  it('Item quality not < 0', () => {
    let store = new GildedRose();

    for (let i = 0; i < 100; i++) {
      store.updateQuality();
    }

    let dexterityVest = getItem(store, '+5 Dexterity Vest');
    let elixir = getItem(store, 'Elixir of the Mongoose');

    expect(dexterityVest?.quality).toEqual(0);
    expect(elixir?.quality).toEqual(0);
  });

  it('Normal item degrades twice as fast after sellIn 0', () => {
    let store = new GildedRose();

    store.addItem('Normal item test', 5, 20, ItemType.Normal);

    for (let i = 0; i < 10; i++) {
      store.updateQuality();
    }

    let normal = getItem(store, 'Normal item test');

    expect(normal?.quality).toEqual(5);
  });

  it('Conjured mana cake is 2 after 2 updates', () => {
    let store = new GildedRose();

    for (let i = 0; i < 2; i++) {
      store.updateQuality();
    }

    let conjuredTest = getItem(store, 'Conjured Mana Cake');

    expect(conjuredTest?.quality).toEqual(2);
  });

  it('Conjured items degrade twice as fast as Normal items', () => {
    let store = new GildedRose();

    store.addItem('Conjured item test', 5, 50, ItemType.Conjured);

    for (let i = 0; i < 10; i++) {
      store.updateQuality();
    }

    let conjuredTest = getItem(store, 'Conjured item test');

    expect(conjuredTest?.quality).toEqual(20);
  });

});

describe('type mapping', () => {
  it('item util properties not null', () => {
    const gildedRose = new GildedRose();

    expect(gildedRose.itemUtil).not.toBeNull();
    expect(gildedRose.itemUtil.typeMap).not.toBeNull();
    expect(gildedRose.itemUtil.items).not.toBeNull();
  });

  it('items and itemUtil.items equal', () => {
    const gildedRose = new GildedRose();

    expect(gildedRose.itemUtil.items).toEqual(gildedRose.items);
  });

  it('items and itemUtil.items equal after adding', () => {
    const gildedRose = new GildedRose();
    gildedRose.addItem('An item', 5, 5, ItemType.Normal);
    gildedRose.items.push(new Item('Another item', 5, 5));

    expect(gildedRose.itemUtil.items.length).toEqual(gildedRose.items.length);
  });

  it('typeMap contains all values', () => {
    const gildedRose = new GildedRose();
    
    expect(gildedRose.itemUtil.typeMap.size).toEqual(6);
  });

  it('typeMap overwrites existing', () => {
    const gildedRose = new GildedRose();

    gildedRose.addItem('An item', 5, 5, ItemType.Normal);
    gildedRose.addItem('An item', 5, 5, ItemType.Legendary);
    
    expect(gildedRose.itemUtil.typeMap.size).toEqual(7);
    expect(gildedRose.itemUtil.typeMap.get('An item')).toEqual(ItemType.Legendary);
  });

  it('getType returns expected type', () => {
    const gildedRose = new GildedRose();

    expect(gildedRose.itemUtil.getType('Sulfuras, Hand of Ragnaros')).toEqual(ItemType.Legendary);
  });

  it('unknown type defaults to Normal', () => {
    const gildedRose = new GildedRose();

    expect(gildedRose.itemUtil.getType('Wacky Name Here')).toEqual(ItemType.Normal);
  });

});
