export class Item {
  name: string;
  sellIn: number;
  quality: number;

  constructor(name: string, sellIn: number, quality: number) {
    this.name = name;
    this.sellIn = sellIn;
    this.quality = quality;
  }
}

export enum ItemType {
  Normal,
  Vintage,
  Legendary,
  Backstage,
  Conjured
} 

class ItemUtil {
  typeMap: Map<string, ItemType>;
  items: Item[];

  constructor(items: Item[]) {
    this.typeMap = new Map<string, ItemType>();
    this.items = items;
  }

  addItem(name: string, sellIn: number, quality: number, itemType: ItemType) {
    let item = new Item(name, sellIn, quality);
    this.items.push(item);
    this.typeMap.set(name, itemType);
  }

  getType(name: string): ItemType {
    let itemType = this.typeMap.get(name);

    // If an itemType with specified name is not found, default to Normal item type
    return itemType || ItemType.Normal;
  }


}

export default class GildedRose {
  items: Item[];
  itemUtil: ItemUtil;

  constructor() {
    this.items = [];
    this.itemUtil = new ItemUtil(this.items);

    this.addItem('+5 Dexterity Vest', 10, 20, ItemType.Normal);
    this.addItem('Aged Brie', 2, 0, ItemType.Vintage);
    this.addItem('Elixir of the Mongoose', 5, 7, ItemType.Normal);
    this.addItem('Sulfuras, Hand of Ragnaros', 0, 80, ItemType.Legendary);
    this.addItem('Backstage passes to a TAFKAL80ETC concert', 15, 20, ItemType.Backstage);
    this.addItem('Conjured Mana Cake', 3, 6, ItemType.Conjured);
  }

  addItem(name: string, sellIn: number, quality: number, itemType: ItemType) {
    this.itemUtil.addItem(name, sellIn, quality, itemType);
  }

  updateQuality(): this {

    this.items.forEach(item => {
      let itemType = this.itemUtil.getType(item.name);

      switch (itemType) {
        case ItemType.Normal:
          if (item.sellIn > 0) {
            item.quality--;
          } else {
            item.quality -= 2;
          }
          break;
        case ItemType.Vintage:
          item.quality++;
          break;
        case ItemType.Backstage:
          if (item.sellIn <= 0) {
            item.quality = 0;
          }
          else if (item.sellIn <= 5) {
            item.quality += 3;
          } else if (item.sellIn <= 10) {
            item.quality += 2;
          } else if (item.sellIn > 10) {
            item.quality++;
          }
          break;
        case ItemType.Conjured:
          if (item.sellIn > 0) {
            item.quality -= 2;
          } else {
            item.quality -= 4;
          }

          break;
        case ItemType.Legendary:
          break;
      
        default:
          break;
      }

      if (itemType !== ItemType.Legendary) {
        item.sellIn--;

        if (item.quality < 0) {
          item.quality = 0;
        }
  
        if (item.quality > 50) {
          item.quality = 50;
        }
      }
    });

    return this;
  }
}
